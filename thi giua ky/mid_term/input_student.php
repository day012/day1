<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="register.css">
</head>

<body>
<div class="container">
    <h1>Form đăng ký sinh viên</h1>
    <div id = "errorMessage"></div>

    <form action="">
        <label for="inputname" class="input_name">Họ và tên </label>
        <input type="text" name="inputname" class="entering" required><br><br>
    </form>

    <form action="">
        <label for="gioitinh" class="input_name input_gen">Giới tính </label>
        <div class="choose_gender">
            <label for="male" class="checkbox-label">
                <input type="radio" id="male" name="gender" value="Nam">
                Nam
            </label>
            <label for="female" class="checkbox-label">
                <input type="radio" id="female" name="gender" value="Nữ">

                Nữ
            </label>
        </div>
    </form>



    <form action="">
        <label class="input_name date_of_birth">Ngày sinh </label>
            <label for="namsinh" class="sub_label">Năm</label>
            <select name="phankhoa" class="choose-falcuty" id="namsinh" required >
                <option value=""></option>
                <?php
                for ($year = 1983; $year <= 2008; $year++) {
                    echo '<option value="' . $year . '">' . $year . '</option>';
                }
                ?>
            </select>
            <label for="thangsinh" class="sub_label">Tháng</label>
            <select name="phankhoa" class="choose-falcuty" id="thangsinh" required>
                <option value=""></option>
                <?php
                for ($month = 1; $month <= 12; $month++) {
                    echo '<option value="' . $month . '">' . $month . '</option>';
                }
                ?>
            </select>
            <label for="ngaysinh" class="sub_label">Ngày</label>
            <select name="phankhoa" class="choose-falcuty"  id="ngaysinh" required>
                <option value=""></option>
                <?php
                for ($date = 1; $date <= 31; $date++) {
                    echo '<option value="' . $date . '">' . $date . '</option>';
                }
                ?>
            </select>
    </form>
    <form action="">
        <label class="input_name date_of_birth">Địa chỉ </label>
        <label for="thanhpho" class="sub_label">Thành phố</label>
        <select name="phankhoa" class="choose-address" id="thanhpho" required >
            <option value=""></option>
            <option value="HaNoi">Hà Nội</option>
            <option value="TpHoChiMinh">Tp. Hồ Chí Minh</option>
        </select>
        <label for="quan" class="sub_label">Quận</label>
        <select name="phankhoa" class="choose-address" id="quan" required>
            <option value=""></option>
        </select>

    </form>
    <form action="">
        <label for="address" class="input_name address"> Thông tin khác </label>
        <textarea id="address" name="address" class="input_address" rows="4" cols="30"></textarea>
    </form>



    <button class="button-container" id="submitButton"> Đăng ký </button>

</div>

<script type="text/javascript" src="index.js"></script>
<script>
    // Lấy các phần tử select box
    var thanhPhoSelect = document.getElementById("thanhpho");
    var quanSelect = document.getElementById("quan");

    // Định nghĩa các quận tương ứng với từng thành phố
    var quanHaNoi = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
    var quanTpHoChiMinh = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];

    // Xử lý sự kiện khi select box "Thành phố" thay đổi
    thanhPhoSelect.addEventListener("change", function() {
        // Xóa tất cả các option trong select box "Quận"
        quanSelect.innerHTML = '<option value="">-- Chọn quận --</option>';

        // Lấy giá trị của select box "Thành phố"
        var thanhPho1 = thanhPhoSelect.value;
        var thanhPho = thanhPho1.trim()

        // Tạo các option cho select box "Quận" tương ứng với thành phố
        if (thanhPho === "Hà Nội") {
            for (var i = 0; i < quanHaNoi.length; i++) {
                var option = document.createElement("option");
                option.text = quanHaNoi[i];
                option.value = quanHaNoi[i];
                quanSelect.add(option);
            }
        } else if (thanhPho === "Tp. Hồ Chí Minh") {
            for (var i = 0; i < quanTpHoChiMinh.length; i++) {
                var option = document.createElement("option");
                option.text = quanTpHoChiMinh[i];
                option.value = quanTpHoChiMinh[i];
                quanSelect.add(option);
            }
        }
    });
</script>
</body>

</html>
