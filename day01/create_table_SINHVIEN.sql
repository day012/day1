
CREATE TABLE SinhVien (
    MaSV VARCHAR(6) ,
    HoSV VARCHAR(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int,
    PRIMARY KEY (MaSV),
    Foreign key (MaKH) references DMKHoa(MaKH)	
);
