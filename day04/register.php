<?php
// Mảng lưu thông tin phân khoa
$phanKhoa = array(
    "--Chọn phân khoa--",
    "Khoa học máy tính",
    "Khoa học vật liệu"
);

// Mảng lưu thông tin giới tính
$gioiTinh = array(
    0 => "Nam",
    1 => "Nữ"
);

// Kiểm tra khi form được gửi đi
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $selectedGioiTinh = isset($_POST["gioiTinh"]) ? $_POST["gioiTinh"] : array();
    $selectedKhoa = $_POST["khoa"];

    // Xử lý thông tin được chọn
    // ...

    // Hiển thị thông tin đã chọn

}

?>





<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>
        /* CSS để cải thiện giao diện của trang đăng nhập */

        p {
            background-color: #f2f2f2;
            padding: 8px;
            width: 76%;
            margin-left: 1cm;
        }

        body {
            font-family: Arial, sans-serif;
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;

        }

        form {
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            border: 2px solid #007bff;
            /* Thêm viền xanh nước biển */

        }
        .erros{
            color: red;
            display: flex;
            align-items: center;
            text-align: center;
            position: relative;
            left: 65px;
            margin-top: 7px;
            margin-bottom: 7px;
        }

        .username_label {
            font-weight: bold;
            background-color: green;
            padding: 10px;
            margin: 2px 0;
            color: white;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            width: 90px;
            height: 15px;
            margin-right: 0.5cm;
            margin-left: 1cm;
            border: 2px solid blue;
            position: relative;
        }
        .username_label::after{
            content: "*";
            color: red;
            position: absolute;
            top: 0;
            right: 10px;
        }
        .username{
            width: 39%;
            height: 35px;
            border: 2px solid blue;
            display: inline-flex;
        }

        .checkbox-label {
            position: relative;
            top: 10px;
            padding-left: 30px;
            padding-right: 30px;
            cursor: pointer;
        }

        .checkbox-label input {
            display: none;
        }

        .checkbox-custom {
            position: absolute;
            top: 0;
            left: 0;
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background-color: green;
            border: 2px solid blue;
        }

        .checkbox-label input:checked + .checkbox-custom:before {
            content: "";
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 10px;
            height: 10px;
            border-radius: 50%;
            background-color: #000;
        }
        .container{
            display: flex;
            padding-bottom: 20px;
        }

        .container_2{
            padding-bottom: 20px;
        }

        .phankhoa{
            width: 20%;
            height: 35px;
            border: 2px solid blue;
            display: inline-flex;
        }
        .address_label {
            font-weight: bold;
            background-color: green;
            padding: 10px;
            margin: 2px 0;
            color: white;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            width: 90px;
            height: 15px;
            margin-right: 0.5cm;
            margin-left: 1cm;
            border: 2px solid blue;
            vertical-align: top;
            position: relative;
        }


        .address{
            width: 39%;
            height: 100px;
            border: 2px solid blue;
            display: inline-flex;
            word-wrap: break-word;
        }

        input[type="submit"] {
            background-color: green;
            color: #fff;
            padding: 10px 40px;
            border: none;
            border-radius: 5px;
            border-style;
            cursor: pointer;
            font-weight: bold;
            margin-top: 10px;
            text-align: center;
            border: 2px solid blue;
            display: block;
            /* Để đặt chiều rộng và căn giữa */
            margin: 0 auto;
            /* Căn giữa ngang */
        }
    </style>

</head>

<body>
<div>
    <h1></h1>
    <form  method="POST" id="registration-form">

        <span class="erros" id="username-erros"></span>
        <span class="erros" id="major-erros"></span>
        <span class="erros" id="date-empty-erros"></span>
        <span class="erros" id="date-invalid-erros"></span>
        <label class="username_label" for="username">Họ và tên</label>
        <input type="text" id="username" name="username" class="username"><br><br>
        <div class="container">
            <label class="username_label" for="checkbox">Giới tính</label>
            <div class="form-checkbox" id="checkbox">
                <label for="checkbox_1" class="checkbox-label">
                    <input type="checkbox" class="checkbox" id="checkbox_1" name="gioiTinh[]" value="0" />
                    <span class="checkbox-custom"></span>
                    Nam
                </label>

                <label for="checkbox_2" class="checkbox-label">
                    <input type="checkbox" class="checkbox" id="checkbox_2" name="gioiTinh[]" value="1" />
                    <span class="checkbox-custom"></span>
                    Nữ
                </label>

            </div>
        </div>
        <div class="container_2">
            <label class="username_label" for="khoa">Phân khoa</label>
            <select name="khoa" id="khoa" class="phankhoa">
                <?php
                foreach ($phanKhoa as $pk) {
                    echo "<option value='" . $pk . "'>" . $pk . "</option>";
                }
                ?>
            </select>
        </div>
        <label class="username_label" for="date_of_birth">Ngày sinh</label>
        <input type="text" id="date_of_birth" class="phankhoa" ><br><br>
        <label class="address_label" for="address">Địa chỉ</label>
        <textarea id="address" class="address" ></textarea><br><br>


        <input type="submit" value="Đăng ký">
    </form>


</div>
</body>
<script src="index.js"></script>

</html>
