var username = document.querySelector('#username')

var major = document.querySelector('#khoa')
var date =document.querySelector('#date_of_birth')
var form = document.querySelector('form')

function showNameErros(input, message){
    input.value =input.value.trim()
    let small = document.querySelector('#username-erros');
    if(!input.value) {

        small.innerText = message
    }
    else{
        small.innerText = '';
    }

}
function showmajorErros(input, message){
    input.value =input.value.trim()
    let small = document.querySelector('#major-erros');
    if(input.value=='--Chọn phân khoa--') {
        small.innerText = message
    }
    else{
        small.innerText = '';
    }
}
function showdateempErros(input, message){
    input.value =input.value.trim()
    let small = document.querySelector('#date-empty-erros');
    if(!input.value) {
        small.innerText = message
    }
    else{
        small.innerText = '';
    }

}


function checkdate(input, message){
    var regexDate = /^\d{2}\/\d{2}\/\d{4}$/;
    let small = document.querySelector('#date-invalid-erros');
    if(!regexDate.test(input.value)){

        small.innerText = message
    }
    else{
        small.innerText = ''
    }
}
form.addEventListener('submit', function (e){
    e.preventDefault()

    showNameErros(username, 'Hãy nhập tên .')
    showmajorErros(major, 'Hãy chọn phân khoa .')
    showdateempErros(date, 'Hãy nhập ngày sinh .')
    checkdate(date, 'Hãy nhập ngày sinh đúng định dạng .')
})
