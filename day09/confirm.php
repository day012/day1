
<script>
    document.getElementById('name').textContent = localStorage.getItem("name")
    document.getElementById('gender').textContent = localStorage.getItem("gender")
    document.getElementById('khoa').textContent = localStorage.getItem("phankhoa")
    document.getElementById('dob').textContent = localStorage.getItem("ngaysinh")

    address = localStorage.getItem("address");
    var url = localStorage.getItem('my-image');

    if (address !== '') {
        document.getElementById('address').textContent = address;
    } else {
        document.getElementById('address').textContent = "Địa chỉ trống";
    }

    document.getElementById('image').src = url
    <?php
        $img = $_POST['url'];
    ?>
</script>
</body>
</html>
<?php
global $connect;
include 'database.php';

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $name = $_POST["name"];
    $gender = $_POST["gender"];
    $major = $_POST["major"];
    $dob = $_POST["dob"];
    $addr = $_POST["adr"];

    // Chuẩn bị truy vấn SQL để chèn dữ liệu vào bảng students
    $sql = "INSERT INTO students (fullname, gender,department,birthdate, address, image) 
            VALUES ('$name','$gender' ,'$major','$dob', '$addr', '$img')";

    // Thực thi truy vấn
    $connect -> exec($sql);
}
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận đăng ký</title>
    <link rel="stylesheet" href="confirm.css">
</head>
<body>

<form method="POST" action = "database.php" enctype="multipart/form-data">
    <div class="container">
        <div id = "errorMessage"></div>

        <div class="input">
            <label for="inputname" class="input_name">Họ và tên</label>
            <p id="name" name="name"></p>
        </div>

        <div class="input">
            <label for="gioitinh" class="input_name input_gen">Giới tính</label>
            <p id="gender" name="gender"></p>
        </div>

        <div class="input">
            <label for="phankhoa" class="input_name input_falcuty">Phân khoa</label>
            <p id="khoa" name="major"></p>
        </div>

        <div class="input">
            <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh </label>
            <p id="dob" name="dob"></p>
        </div>

        <div class="input">
            <label for="address" class="input_name address"> Địa chỉ </label>
            <p id="address" name="adr"></p>
        </div>

        <div class="input">
            <label for="image" class="input_name uploadImage"> Hình ảnh </label>
            <img id="image" src="" alt="Ảnh">
        </div>

        <button class="button-container" id="confirmButton"> Xác nhận</button>
    </div>
</form>

