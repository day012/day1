function formatDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }

    return day + "/" + month + "/" + year;
}

function parseDate(dateString) {
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10) - 1;
    var year = parseInt(parts[2], 10);

    return new Date(year, month, day);
}


function goToPage(pageUrl) {
    window.location.href = pageUrl
}




//Xu ly submit button

if (document.getElementById('submitButton') !== null) {
    document.getElementById('submitButton').addEventListener('click', function() {

        var ngaysinhInput = document.getElementById("dob");

        ngaysinhInput.addEventListener("blur", function() {
            var enteredDate = ngaysinhInput.value;
            var formattedDate = formatDate(parseDate(enteredDate));
            ngaysinhInput.value = formattedDate;
        });

        var inputName = document.querySelector('.entering');

        var genderInputs = document.querySelectorAll('input[name="gender"]');
        var selectedGender = Array.from(genderInputs).find(input => input.checked);
        var gender = ""
        genderInputs.forEach(function(input) {
            if (input.checked) {
                gender = input.value;
            }
        });

        var selectedFaculty = document.querySelector('.choose-falcuty');
        var inputDate = document.querySelector('.date_input');
        var errorMessage = document.getElementById('errorMessage');

        errorMessage.innerHTML = '';

        if (inputName.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập tên.<br>';
        }
        if (!selectedGender) {
            errorMessage.innerHTML += 'Hãy chọn giới tính <br>';
        }
        if (selectedFaculty.value === '') {
            errorMessage.innerHTML += 'Hãy chọn phân khoa.<br>';
        }
        if (inputDate.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập ngày sinh<br>';
        }
        else {
            var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;

            if (!dateRegex.test(inputDate.value)) {
                errorMessage.innerHTML += 'Hãy nhập ngày sinh đúng định dạng<br>';
            }
        }

        if (errorMessage.innerHTML !== '') {
            errorMessage.style.display = 'block';
            console.log("Đăng ký lỗi!")
        }
        else {
            errorMessage.style.display = 'none';

            localStorage.setItem("name", inputName.value);
            localStorage.setItem("gender", gender);
            localStorage.setItem("phankhoa", selectedFaculty.value);
            localStorage.setItem("ngaysinh", inputDate.value.trim());
            localStorage.setItem("address", document.getElementById('address').value)

            var imageInput = document.getElementById('image');
            if (imageInput.files.length > 0) {

                const fr = new FileReader();

                var selectedImage = imageInput.files[0];

                fr.readAsDataURL(selectedImage)

                fr.addEventListener('load', () => {
                    const url = fr.result;

                    localStorage.setItem('my-image', url);
                });

            }

            console.log("Đăng ký thành công!");
            goToPage("confirm.php");
        }
    });
}

