<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name ="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title></title>

</head>

<body>
<div class="container">
    <form>
        <label for="inputname" class="input_name">Khoa </label>
        <select id="department" name="department" class="entering">
            <option value="" >--Chọn phân khoa--</option>
            <?php
            $departments = [
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];

            foreach ($departments as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
            ?>
        </select><br><br>
    </form>

    <form>
        <label for="inputmajor" class="input_name">Từ Khóa </label>
        <input type="text" name="inputname" class="entering" id="inputmajor" ><br><br>
    </form>
    <button class="button-container" id="nonsubmitButton" > Reset </button>
    <button class="button-container" id="nonsubmitButton_2"> Tìm kiếm </button>
    <button class="button-container" id="submitButton" > Thêm </button>
    <div class="tittle">
        <?php
        require 'database.php';
        global $connect;
        $countSql = "SELECT COUNT(*) as total FROM students";
        $countResult = $connect->query($countSql);
        $row = $countResult->fetch_assoc();
        $totalStudents = $row['total'];

        echo "<strong>Số sinh viên tìm thấy: " . $totalStudents . "</strong>";

        $connect->close();
        ?>
    </div>
    <div>
        <table>
            <thead>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php

            function writedpm($row){
                if($row=="MAT"){
                    $temp="Khoa học máy tính";
                }
                else{
                    $temp="Khoa học vật liệu";
                }
                return $temp;
            }
            global $connect;
            include 'database.php';
            $sql = "SELECT fullname, department FROM students";
            $result = $connect->query($sql);

            $i = 1 ;
            while($row = $result->fetch_assoc()){
                echo "<tr>";
                echo "<td>" . $i . "</td>";
                echo "<td>" . $row["fullname"]. "</td>";
                echo "<td>" . writedpm($row["department"]). "</td>";
                echo "<td>";
                echo '<button class="button-container" onclick="openPopup()" id="tableButton">Xóa</button>' ;
                echo '<button class="button-container" id="tableButton">Sửa</button>' ;
                echo "</td>";
                echo "</tr>";
                $i++;
            }

            $connect->close();
            ?>
            </tbody>
        </table>
    </div>
    <div id="popup" style="display:none">
        <div class="popup-content">
            <p>Bạn muốn xóa sinh viên này?</p>
            <button id="cancel" class="action-button" onclick="closePopup()">Hủy</button>
            <button id="delete" class="action-button">Xóa</button>
        </div>
    </div>
</div>

</body>
<script>
    function goToPage(pageUrl) {
        window.location.href = pageUrl;
    }

    if (document.getElementById('submitButton') !== null) {
        document.getElementById('submitButton').addEventListener('click', function() {
            goToPage("register.php");
        });
    }
    if(document.getElementById('nonsubmitButton') !== null){
        document.getElementById('nonsubmitButton').addEventListener('click', function(){
            document.getElementById('department').value = " ";
            document.getElementById('inputmajor').value = " ";
        });
    }

        // Open popup function
        function openPopup() {
        let btn = event.target;
        let studentId = btn.dataset.id;
        document.getElementById("popup").style.display = "block";
        // Xoá sinh viên với ID
        $('#delete').click(function(){

        $.ajax({
        url: 'delete-student.php',
        type: 'POST',
        data: {id: studentId}
    })
        .done(function(response){
        closePopup();
        // Load lại trang sau 1 giây
        setTimeout(function(){
        location.reload();

    }, 1000);

    })

    })
    }
        // Close popup function
        function closePopup() {
        document.getElementById("popup").style.display = "none";
    }

</script>
<script>
    $(document).ready(function () {
        // Chuyển mảng $departments thành biến JavaScript
        var departments = <?php echo json_encode($departments); ?>;


        $("#department, #inputmajor").on("input", function () {
            searchAndPopulateTable();
        });

        function searchAndPopulateTable() {
            var departmentCode = $("#department").val();
            var keyword = $("#inputmajor").val();

            // Lấy tên khoa tương ứng từ biến JavaScript
            var departmentName = departments[departmentCode];

            $.ajax({
                url: "search.php",
                method: "POST",
                data: { department: departmentName, keyword: keyword },
                success: function (data) {
                    $(".action-buttons").html(data);
                }
            });
        }

        function resetTableData() {
            $.ajax({
                url: "reset.php",
                method: "POST",
                success: function (data) {
                    $(".action-buttons").html(data);
                }
            });
        }
    });
</script>
</html>


