<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="register.css">
</head>

<body>
<div class="container">
    <div id = "errorMessage"></div>

    <form action="">
        <label for="inputname" class="input_name">Họ và tên <span class="required">*</span></label>
        <input type="text" name="inputname" class="entering" required><br><br>
    </form>

    <form action="">
        <label for="gioitinh" class="input_name input_gen">Giới tính <span class="required">*</span></label>
        <div class="choose_gender">
            <label for="male" class="checkbox-label">
                <input type="checkbox" id="male" name="gender" value="Nam" class="checkbox">
                <span class="checkbox-custom"></span>
                Nam
            </label>
            <label for="female" class="checkbox-label">
                <input type="checkbox" id="female" name="gender" value="Nữ">
                <span class="checkbox-custom"></span>
                Nữ
            </label>
        </div>
    </form>

    <form action="">
        <label for="phankhoa" class="input_name input_falcuty">Phân khoa <span class="required">*</span></label>
        <select name="phankhoa" class="choose-falcuty" required>
            <option value="">-- Chọn phân khoa --</option>
            <option value="KHMT">Khoa học máy tính </option>
            <option value="KHDL">Khoa học dữ liệu </option>
        </select>
    </form>

    <form action="">
        <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh <span class="required">*</span></label>
        <input type="text" id="dob" name="ngaysinh" class="date_input" placeholder="dd/mm/yyyy" required>
    </form>

    <form action="">
        <label for="address" class="input_name address"> Địa chỉ </label>
        <textarea id="address" name="address" class="input_address" rows="4" cols="30"></textarea>
    </form>

    <form action="">
        <label for="image" class="input_name uploadImage"> Hình ảnh </label>
        <input id="image" type="file" name="fileToUpload" class="fileToUpload">
    </form>

    <button class="button-container" id="submitButton"> Đăng ký </button>

</div>

<script type="text/javascript" src="script.js"></script>
</body>

</html>