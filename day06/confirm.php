<?php
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận đăng ký</title>
    <link rel="stylesheet" href="confirm.css">
</head>
<body>

<form action="upload.php" method="post"  enctype="multipart/form-data">
    <div class="container">
        <div id = "errorMessage"></div>

        <div class="input">
            <label for="inputname" class="input_name">Họ và tên</label>
            <p id="name" name="name"></p>
        </div>

        <div class="input">
            <label for="gioitinh" class="input_name input_gen">Giới tính</label>
            <p id="gender" name="gender"></p>
        </div>

        <div class="input">
            <label for="phankhoa" class="input_name input_falcuty">Phân khoa</label>
            <p id="khoa" name="major"></p>
        </div>

        <div class="input">
            <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh </label>
            <p id="dob" name="dob"></p>
        </div>

        <div class="input">
            <label for="address" class="input_name address"> Địa chỉ </label>
            <p id="address" name="adr"></p>
        </div>

        <div class="input">
            <label for="image" class="input_name uploadImage"> Hình ảnh </label>
            <img id="image" src="" alt="Ảnh">
        </div>

        <button class="button-container" id="confirmButton"> Xác nhận</button>
    </div>
</form>

<script type="text/javascript" src="confirm_script.js"></script>
</body>
</html>