CREATE DATABASE ltweb
CREATE TABLE students (
    name VARCHAR(50), 
    gender VARCHAR(50),
    major VARCHAR(50), 
    dob VARCHAR(50),
    address VARCHAR(50), 
    image_url VARCHAR(50)
);

