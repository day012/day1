<?php

$sname = 'mysql:host=localhost;dbname=ltweb;charset=utf8';
$uname = "root";
$password = "";

$db_name = "ltweb";

$conn = mysqli_connect($sname, $uname, $password, $db_name);

if (!$conn) {
    echo "Connection failed!";
    exit();
}
