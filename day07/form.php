<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name ="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title></title>

</head>

<body>
<div class="container">
    <form>
        <label for="inputname" class="input_name">Khoa </label>
        <input type="text" name="inputname" class="entering" required><br><br>
    </form>
    <form>
        <label for="inputname" class="input_name">Từ Khóa </label>
        <input type="text" name="inputname" class="entering" required><br><br>
    </form>
    <button class="button-container" id="nonsubmitButton" > Tìm kiếm </button>
    <button class="button-container" id="submitButton"> Thêm </button>
    <p class="tittle">Số sinh viên tìm thấy : XXX</p>
    <div>
        <table>
            <thead>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            global $connect;
            include 'database.php';
            $sql = "SELECT fullname, department FROM students";
            $result = $connect->query($sql);

            $i = 1 ;
            while($row = $result->fetch_assoc()){
                echo "<tr>";
                echo "<td>" . $i . "</td>";
                echo "<td>" . $row["fullname"]. "</td>";
                echo "<td>" . $row["department"]. "</td>";
                echo "<td>";
                echo '<button class="button-container" id="tableButton">Xóa</button>' ;
                echo '<button class="button-container" id="tableButton">Sửa</button>' ;
                echo "</td>";
                echo "</tr>";
                $i++;
            }

            $connect->close();
            ?>
            </tbody>
        </table>
    </div>
</div>

</body>
<script>
    function goToPage(pageUrl) {
        window.location.href = pageUrl;
    }

    if (document.getElementById('submitButton') !== null) {
        document.getElementById('submitButton').addEventListener('click', function() {
            goToPage("register.php");
        });
    }
</script>
</html>


